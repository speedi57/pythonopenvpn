#!/usr/bin/env python
# -*- coding:utf-8 -*-

from flask import Flask, request, flash, redirect, url_for
from flask import send_file
from flask import render_template
from flask import session
from module import validateEmail , SQL , autolog , OpenVPNStatusParser
import os
import sys
sys.excepthook = lambda *args: None
global DB 
DB = SQL()
app = Flask(__name__)
app.secret_key = 'd66HR8dç"f_-àgjYYic*dh'
@app.route('/login/', methods=['GET', 'POST'])
def login():
	user = request.user_agent.string
	if user.find("Trident") != -1:
		flash(u"s'il vous plait installer un vrais navigateur --'", 'succes')
		return render_template('internet_explorer.html')
	if request.method == 'POST':
		user = request.form['user']
		psw = request.form['pw']
		test = DB.check(user,psw)
		if test == 1: # on vérifie que le mot de passe est bon
			flash(u'login succesfull', 'succes')
			session['connect'] = "false"
			session['pseudo'] = user
			session['rang'] = DB.getrang(user)
			return redirect('/')
		else:
			flash(u'login failed', 'error')
	return render_template('login.html')
@app.route('/register/', methods=['GET', 'POST'])
def register():
	if request.method == 'POST':
		user = request.form['user']
		psw = request.form['pw']
		email = request.form['email']
		test = DB.userexist(user)
		if test == 1: # on vérifie que le mot de passe est bon
			flash(u'pseudo deja utilisé', 'error')
			return render_template('register.html')
		else:
				test = validateEmail(email)
				if test == 1 :
					if user == "" or psw == "":
						flash(u'utilisateur ou mot de passe non valid', 'error')
						return render_template('register.html')	
					else:	
						DB.registered(user,psw,email)
						flash(u'enregistrement accomplie', 'succes')
						return render_template('register.html')
				else :
					flash(u'email incorrect', 'error')
					return render_template('register.html')				
	return render_template('register.html')
@app.errorhandler(404)
def ma_page_404(error):
	flash(u"cette page n'existe pas qu'essayé vous de faire O.o?", 'error')
	return render_template('error.html')  
@app.route('/', methods=['GET', 'POST'])
def accueil():
	if 'pseudo' in session:
		flash(u'bienvenue '+ session['pseudo'], 'succes')
		return render_template('accueil.html') 
	else:
		session['connect'] = "true"
		session["pseudo"]=""
		return render_template('accueil.html')
@app.route('/logout/')
def logout():
	session.pop('pseudo', None)
	session.pop('rang', None)
	session['connect'] = "true"
	
	return redirect('/')
@app.route('/news/')
def news():
		news = DB.listenews()
		nb = len(news["titre"])
		return render_template('news.html',news=news, nb=nb)
@app.route('/userlist/')
def userlist():
		news = DB.userlist()
		nb = len(news["titre"])
		return render_template('user.html',news=news, nb=nb)
@app.route('/addnews/', methods=['GET', 'POST'])
def addnews():
	if 'rang' in session:
		if session['rang'] == 1:
			if request.method == 'POST':
				titre = request.form['Titre']
				text = request.form['Text']
				DB.postnews(titre,text)
				flash(u'news posté avec succes', 'succes')
			else :
				return render_template('addnews.html')
		else :
			return redirect('/')
	return redirect('/')
@app.route('/autologin/', methods=['GET', 'POST'])
def autologin():
	if request.method == 'POST':
		autolog(request.form['user'],request.form['pw'])
		return send_file("./up/autologin/"+request.form['user']+"/autologin.zip", as_attachment=True)
	else :
		return render_template('autologin1.html')
@app.route('/openvpn86/')
def openvpn86() :
	return send_file("./up/openvpn-install-2.3.4-I001-i686.exe", as_attachment=True)
@app.route('/unroot/')
def unroot() :
	return send_file("./up/HMA_UNrouting_Utility.zip", as_attachment=True)
@app.route('/openvpn64/')
def openvpn64():
	return send_file("./up/openvpn-install-2.3.4-I001-x86_64.exe", as_attachment=True)
@app.route('/conf_client/')
def conf_client():
	return send_file("./static/conf_client.zip", as_attachment=True)
@app.route('/stats/')
def statvpn():
	if 'rang' in session:
		if session['rang'] == 1:
			files = ["/etc/openvpn/openvpn-status.log"]
			client =[]
			received=[]
			send=[]
			address =[]
			i = 0
			for file in files:
				stat = OpenVPNStatusParser(file)
				for i in stat.connected_clients:
					address += stat.connected_clients[i]['Real Address']
					address +=";"
					client += i
					client += ";"
					received += stat.connected_clients[i]['Bytes Received']
					received += ";"
					send += stat.connected_clients[i]['Bytes Sent']
					send += ";"
				send = change(send)
				received = change(received)
				client=change(client)
				address =change(address)
				print client 
				stat.routing_table
				stat.details
				nb = len(client)
			return render_template('stats.html',stat=client,address=address,nb=nb,send = send , received =received)
		else:
			return redirect('/')
	else :
		return redirect('/')
	
def change(address):
	address ="".join(address)
	address = address.split(";")
	return address
if __name__ == '__main__':
   app.run(debug=True,port = 80,host = "localhost")
