CREATE DATABASE  IF NOT EXISTS `openvpn` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `openvpn`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: localhost    Database: openvpn
-- ------------------------------------------------------
-- Server version	5.5.37-0+wheezy1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `log_trusted_ip` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `log_trusted_port` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `log_remote_ip` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `log_remote_port` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `log_start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `log_end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `log_received` float NOT NULL DEFAULT '0',
  `log_send` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`log_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log`
--

LOCK TABLES `log` WRITE;
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
INSERT INTO `log` VALUES (1,'test','77.203.155.243','2694','10.8.0.6','443','2014-05-27 19:09:57','2014-05-27 19:12:18',8115,4540),(2,'test','77.203.155.243','2832','10.8.0.6','443','2014-05-27 19:12:31','2014-05-27 19:13:30',7787,4100),(3,'test','77.203.155.243','2873','10.8.0.6','443','2014-05-27 19:14:27','2014-05-27 19:17:26',8640,4992),(4,'test','77.203.155.243','3054','10.8.0.6','443','2014-05-27 19:19:26','2014-05-27 19:20:14',11434,4141),(5,'test','77.203.155.243','3081','10.8.0.6','443','2014-05-27 19:20:34','2014-05-27 19:21:02',14881,4031),(6,'test','77.203.155.243','3235','10.8.0.6','443','2014-05-27 19:25:23','2014-05-27 19:26:08',21801,4141),(7,'test','77.203.155.243','3318','10.8.0.6','443','2014-05-27 19:26:43','2014-05-27 19:28:06',16395,4477),(8,'test','77.203.155.243','3428','10.8.0.6','443','2014-05-27 19:30:18','2014-05-27 19:33:50',65297,5137),(9,'test','77.203.155.243','3667','10.8.0.6','443','2014-05-27 19:36:34','2014-05-27 19:37:58',21253,4345),(10,'test','77.203.155.243','3713','10.8.0.6','443','2014-05-27 19:38:16','2014-05-27 19:38:51',9945,4318),(11,'test','77.203.155.243','3759','10.8.0.6','443','2014-05-27 19:39:50','2014-05-27 19:41:36',26078,4487),(12,'test','77.203.155.243','3890','10.8.0.6','443','2014-05-27 19:43:32','2014-05-27 19:44:10',12167,4118),(13,'test','77.203.155.243','3926','10.8.0.6','443','2014-05-27 19:46:34','2014-05-27 19:46:57',10978,4079),(14,'test','77.203.155.243','3988','10.8.0.6','443','2014-05-27 19:47:40','2014-05-27 19:48:22',12257,4134),(15,'test','77.203.155.243','4002','10.8.0.6','443','2014-05-27 19:48:33','2014-05-27 19:49:25',17822,4289),(16,'test','77.203.155.243','4074','10.8.0.6','443','2014-05-27 19:49:41','2014-05-27 19:50:56',46837,4818),(17,'test','77.203.155.243','4169','10.8.0.6','443','2014-05-27 19:51:08','2014-05-27 19:52:11',20976,4367),(18,'test','77.203.155.243','4223','10.8.0.6','443','2014-05-27 19:52:24','2014-05-27 19:52:55',7065,4031),(19,'test','77.203.155.243','4337','10.8.0.6','443','2014-05-27 19:55:49','2014-05-27 19:57:22',27752,4872),(20,'test','77.203.155.243','4392','10.8.0.6','443','2014-05-27 19:57:33','2014-05-27 19:58:41',17683,4283),(21,'test','77.203.155.243','4537','10.8.0.6','443','2014-05-27 20:03:23','2014-05-27 20:06:24',7180,4856),(22,'test','77.203.155.243','4681','10.8.0.6','443','2014-05-27 20:06:43','2014-05-27 20:07:09',10595,4047),(23,'test','83.99.5.183','1555','10.8.0.6','443','2014-05-28 07:06:28','2014-05-28 07:07:00',10454,4102),(24,'test','83.99.5.183','1711','10.8.0.6','443','2014-05-28 07:09:22','2014-05-28 07:10:08',13851,4157),(25,'test','83.99.5.183','1778','10.8.0.6','443','2014-05-28 07:17:51','2014-05-28 07:20:24',13827,4707),(26,'test','83.99.5.183','1897','10.8.0.6','443','2014-05-28 07:23:38','2014-05-28 07:28:28',1051740,4833190);
/*!40000 ALTER TABLE `log` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-05-28 10:07:40
