import re
import zipfile
import shutil
import configparser
import os
def validateEmail(email):
	if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", email) != None:
		return 1
	return 0
def autolog(user,password):
	newpath = r"./up/autologin/"+user
	if not os.path.exists(newpath): os.makedirs(newpath)
	shutil.copyfile("./static/autologin.zip", "./up/autologin/"+user+"/autologin.zip")
	shutil.copyfile("./static/config.ini", "./up/autologin/"+user+"/config.ini")
	config = configparser.ConfigParser()
	config.read("./up/autologin/"+user+"/config.ini")
	config['user_setting']['user'] = user
	config['user_setting']['password'] = password
	with open("./up/autologin/"+user+"/config.ini", 'w') as configfile:    # save
		config.write(configfile)
	try:
		import zlib
		compression = zipfile.ZIP_DEFLATED
	except:
		compression = zipfile.ZIP_STORED
	modes = { zipfile.ZIP_DEFLATED: 'deflated',
			zipfile.ZIP_STORED:   'stored',
			}
	zf = zipfile.ZipFile("./up/autologin/"+user+"/autologin.zip", mode='a')
	try:
		zf.write("./up/autologin/"+user+"/config.ini", arcname='config.ini')
	finally:
		zf.close()
